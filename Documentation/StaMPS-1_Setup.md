The StaMPS-Workflow has been updated. This link is deprecated.

Please refer to [StaMPS-Setup](../StaMPS/1_stamps_setup.md) instead.
