# Table of Contents

* [Introduction](#introductory-comments)
* [About](#1-about-this-document)
* [Setup](#2-setup)
  - [Software](#21-software)
  - [Configuration](#22-configuration)

# Introductory Comments

This document is intended to *provide guidance on how to setup StaMPS for the first time*. 

This document is largely based on the excellent
[*SNAP-StaMPS Workflow*](https://forum.step.esa.int/t/how-to-prepare-sentinel-1-images-stack-for-psi-sbas-in-snap-5/4981/514)
compiled by Thorsten Höser as well as discussions and presented workflows from
[MAINSAR](https://groups.google.com/forum/#!topic/mainsar/38KZ2-6nbrI),
step forum on [snap-stamps workflow](https://forum.step.esa.int/t/workflow-between-snap-and-stamps/3211/9)
and the [stamps-tool](https://forum.step.esa.int/t/about-the-stamps-category/8140)
as well as [Ask Ubuntu](https://askubuntu.com). Thanks to all members and developers
contributing ideas, solving problems and helping to make this workflow as
smooth as possible.

Please note that this summary has been written and tested on a certain machine
using a specific user. Even though it has been tried to generalize code as much
as possible, certain paths may have to be adjusted accordingly.

The used OS is Ubuntu 18.04 LTS.

---

# 1 About this document

Guidance given in this summary is purely technical. It is absolutely
necessary to know the basics of the theoretical background of radar remote
sensing and InSAR:

* Ferretti et al. (2007): [INSAR Principles](http://www.esa.int/About_Us/ESA_Publications/InSAR_Principles_Guidelines_for_SAR_Interferometry_Processing_and_Interpretation_br_ESA_TM-19)
* Review article by Crosetto et al. (2016): [Persistent Scatterer Interferometry: A review](https://www.sciencedirect.com/science/article/pii/S0924271615002415)
* De Zan and Guarnieri (2006): [TOPSAR: Terrain Observation by Progressive Scans](https://ieeexplore.ieee.org/document/1677745?arnumber=1677745)
* Hooper et al. (2007): [Persistent scatterer interferometric synthetic aperture radar for crustal deformation analysis ...](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2006JB004763)
* [Grandin (2015)](http://proceedings.esa.int/page_paper116.php) and [Yagüe-Martínez et al. (2016)](https://ieeexplore.ieee.org/document/7390052) on interferometric processing of SENTINEL-1 TOPS data
* care has to be taken when [using StaMPS in mountainous regions](https://pdfs.semanticscholar.org/b773/af04dfd21f00a079019d5c80d6a1c8051eb3.pdf)
* [EO College](https://www.youtube.com/channel/UCka-UP2X3y3JpJRRvARzEhQ) on YouTube

# 2 Setup
## 2.1 Software
* install [Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-desktop)
* install [SNAP](http://step.esa.int/main/toolboxes/snap/) ([dowload](http://step.esa.int/main/download/))
* install [STAMPS](https://homepages.see.leeds.ac.uk/~earahoo/stamps/) ([download](https://github.com/dbekaert/StaMPS/releases/tag/v4.1-beta))
* install [SNAPHU](https://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/)
  ```bash
  sudo apt-get update
  sudo apt-get install snaphu
  ```
* install [triangle](https://www.cs.cmu.edu/~quake/triangle.html)
  ```bash
  sudo apt-get update
  sudo apt-get install triangle-bin
  ```
* install MATLAB for Linux. Note that this may cause some trouble with respect
  to version incompatibilities of system libraries and libraries contained in
  the MALAB-installation. Possible fixes:
  * upgrade `libstdc++6`-version
    ```bash
    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install gcc-4.9
    sudo apt-get upgrade libstdc++6
    ```
  * rename matlab libdistc++ version to avoid conflicts between matlab
    libraries and system libraries
    ```bash
    mv /usr/local/MATLAB/R2018b/sys/os/glnxa64/libstdc++.so.6 /usr/local/MATLAB/R2018b/sys/os/glnxa64/libstdc++.so.6.old
    ```
  * create symbolic link to my system's libstdc++.so.6
    ```bash
    ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/local/MATLAB/R2018b/bin/glnxa64/../../sys/os/glnxa64/libstdc++.so.6
    ```
  * startup matlab using `LD_PRELOAD` (not feasible)
    ```bash
    LD_PRELOAD="/usr/lib/x86_64-linux-gnu/libstdc++.so.6" matlab
    ```
* install dependencies
  ```bash
  sudo apt-get update
  sudo apt-get install gawk
  sudo apt-get install tcsh
  sudo apt-get install matlab-support
  ```
* download [snap2stamps](https://github.com/mdelgadoblasco/snap2stamps/), or clone the git repo:
  ```bash
  git clone https://github.com/mdelgadoblasco/snap2stamps.git
  ```
* *(optional):* if you intend to include tropospheric correction methods into
  the InSAR processing chain, install the Toolbox for Reducing Atmospheric InSAR
  Noise ([TRAIN](https://github.com/dbekaert/TRAIN)). TRAIN can be
  used in StaMPS step 8 to remove tropospheric delays.
  ```bash
  git clone https://github.com/dbekaert/TRAIN.git
  ```
  Adjust the 
  Note that TRAIN does have additional dependencies (see manual for full description), such as e.g.:
  * [BEAM](https://earth.esa.int/web/sentinel/user-guides/software-tools/-/article/beam) for re-projection of satellite data;
  * [NCL](http://www.ncl.ucar.edu/index.shtml) when using weather model data from the US;
  * [GMT](https://www.soest.hawaii.edu/gmt/) for auxiliary tropospheric correction methods

## 2.2 Configuration
* edit the StaMPS config file (e.g. via `vim ./StaMPS/StaMPS_CONFIG.bash`)
  to contain the correct paths to StAMPS, SNAPHU and triangle:
  ```bash
  # set environment variables:
  export STAMPS="/home/username/StaMPS"
  export SNAP2STAMPS="/home/username/snap2stamps"
  ...
  #if triangle and snaphu are not installed through the repositories (i.e. compiled locally):
  export TRIANGLE_BIN="/home/username/software/triangle/bin"
  export SNAPHU_BIN="/home/username/software/snaphu/bin"
  ...
  export MATLABPATH=$STAMPS/matlab:`echo $MATLABPATH`
  ...
  # use points not commas for decimals, and give dates in US english
  export LC_NUMERIC="en_US.UTF-8"
  export LC_TIME="en_US.UTF-8"
  ...
  export SAR_TAPE="/dev/rmt/0mn"
  ...
  export PATH=${PATH}:$STAMPS/bin:$MATLABPATH:$SNAP2STAMPS/bin

  ```
* source the StaMPS config file upon opening terminal by adding it to your `.bashrc`:
  ```bash
  # source StaMPS
  source /home/username/StaMPS/StaMPS_CONFIG.bash
  ```
* do the same thing for your TRAIN (APS_toolbox) config file:
  * adjust path in the APS_CONFIG file (`vim ./TRAIN/APS_CONFIG.bash`)
    ```bash
    export APS_toolbox="/home/username/TRAIN"
    ```
  * source it upon opening a terminal:
    ```bash
    # add TRAIN config
    source /home/username/TRAIN/APS_CONFIG.sh
    ```

